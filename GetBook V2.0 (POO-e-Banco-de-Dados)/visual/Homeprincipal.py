# -*- coding: utf-8 -*-
'''
Feito em 2019.2
@description: Classe (visual) da tela de home principal do app, ela conteem o botao que leva a tela de aluguel e um de sair 
@author: Waycon Sales
'''
__author__="Waycon Sales"
__credits__=["BibliotecaPy - ", "Academicos: Carliane, Waycon e Kailane"]
__version__="2.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__="15/12/2019"

import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk,Gdk
from visual.AluguelLivro import AluguelLivro
from visual.TabelaAluguel import TabelaAluguel

 

class Homeprincipal:
    #Construtor
    #Instancicao da tela do home principal, da gtk builder...
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("visual/PgPrincipal.glade")
        self.homeP = builder.get_object("tlHomePrincipal")
        builder.connect_signals(self)
        self.homeP.show_all()
        self.css() #execucao do metodo css
        self.homeP.connect("destroy",Gtk.main_quit)
        Gtk.main()
    #metodo da estilizacao da tela, css    
    def css(self):
        css = b"""
         .tlHomePrincipal{
            background-color: #9ab6c8;
            
        }
        .btLista{
            border-radius:50px;
        }
        .btAluguel{
            border-radius:50px;
        }
        .btSair{
            border-radius:50px;
        
        }
        
        
        """
        style_provider = Gtk.CssProvider() #instanciando css provider
        style_provider.load_from_data(css) 
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        
    #metodo do botao sair, sai da aplicacao.
    def sair (self,evt):
        self.homeP.hide()
        self.homeP.destroy()
        
    #metodo do botao aluguel tem a funcao de abrir a tela de AluguelTabela e fechar a do home principal    
    def aluguel (self, evt):
        self.homeP.hide()
        self.homeP.destroy()
        self.alu = AluguelLivro()

    def tabela (self, evt):
        self.homeP.hide()
        self.homeP.destroy()
        self.tabela = TabelaAluguel()    
        
