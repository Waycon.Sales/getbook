# -*- coding: utf-8 -*-
'''
Feito em 2019.2
@description: Classe (visual) da tela de Aluguel De Livros nessa tela encontra-se o formulario onde serao colocadas as informacoes do Aluno e a tabela onde serao mostradas. 
@author: Waycon Sales
'''
__author__="Waycon Sales"
__credits__=["BibliotecaPy - ", "Academicos: Carliane, Waycon e Kailane"]
__version__="2.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__="15/12/2019"

import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk,Gdk
from visual.AluguelLivro import AluguelLivro
from pacoteA.Aluguel import Aluguel
from controle.AluguelControle import AluguelControle


class TabelaAluguel():
    #construtor
    def __init__(self):
        #instanciacao da Gtk
        builder = Gtk.Builder()
        #adiciona o arquivo do glade
        builder.add_from_file("visual/Tabela.glade")
        #objetos da tabela
        self.tabela = builder.get_object("tlTabela")
        self.nomeTabela0 = builder.get_object("lbAluno0")
        self.tituloTabela0 = builder.get_object("lbTitulo0")
        self.editoraTabela0 = builder.get_object("lbEditora0")
        self.emailTabela0 = builder.get_object("lbEmail0")
        self.tellAlunoTabela0 = builder.get_object("lbTelAluno0")
        self.tellPaisTabela0 = builder.get_object("lbTelPais0")
        self.autorTabela0 = builder.get_object("lbAutor0")
        self.dataEntregaTabela0 = builder.get_object("lbEntrega0")
        self.dataEmissaoTabela0 = builder.get_object("lbEmissao0")
        self.cursoTabela0 = builder.get_object("lbCurso0")
        self.serieTabela0 = builder.get_object("lbSerie0")

        self.nomeTabela1 = builder.get_object("lbAluno1")
        self.tituloTabela1 = builder.get_object("lbTitulo1")
        self.editoraTabela1 = builder.get_object("lbEditora1")
        self.emailTabela1 = builder.get_object("lbEmail1")
        self.tellAlunoTabela1 = builder.get_object("lbTelAluno1")
        self.tellPaisTabela1 = builder.get_object("lbTelPais1")
        self.autorTabela1 = builder.get_object("lbAutor1")
        self.dataEntregaTabela1 = builder.get_object("lbEntrega1")
        self.dataEmissaoTabela1 = builder.get_object("lbEmissao1")
        self.cursoTabela1 = builder.get_object("lbCurso1")
        self.serieTabela1 = builder.get_object("lbSerie1")

        self.nomeTabela2 = builder.get_object("lbAluno2")
        self.tituloTabela2 = builder.get_object("lbTitulo2")
        self.editoraTabela2 = builder.get_object("lbEditora2")
        self.emailTabela2 = builder.get_object("lbEmail2")
        self.tellAlunoTabela2 = builder.get_object("lbTelAluno2")
        self.tellPaisTabela2 = builder.get_object("lbTelPais2")
        self.autorTabela2 = builder.get_object("lbAutor2")
        self.dataEntregaTabela2 = builder.get_object("lbEntrega2")
        self.dataEmissaoTabela2 = builder.get_object("lbEmissao2")
        self.cursoTabela2 = builder.get_object("lbCurso2")
        self.serieTabela2 = builder.get_object("lbSerie2")

        #pega os sinais dos botoes
        builder.connect_signals(self)
        #execucao do metodo  css
        self.css()
        aluguelcontrol = AluguelControle()
        #As preenchendo tabelas 
        
        aluno = aluguelcontrol.selecionarAlunos()
        self.nomeTabela0.set_text(str(aluno[0].getNome()))
        self.serieTabela0.set_text(str(aluno[0].getSerie()))
        self.cursoTabela0.set_text(str(aluno[0].getCurso()))
        self.emailTabela0.set_text(str(aluno[0].getEmail()))   
        self.nomeTabela1.set_text(str(aluno[1].getNome()))
        self.serieTabela1.set_text(str(aluno[1].getSerie()))
        self.cursoTabela1.set_text(str(aluno[1].getCurso()))
        self.emailTabela1.set_text(str(aluno[1].getEmail()))
        self.nomeTabela2.set_text(str(aluno[2].getNome()))
        self.serieTabela2.set_text(str(aluno[2].getSerie()))
        self.cursoTabela2.set_text(str(aluno[2].getCurso()))
        self.emailTabela2.set_text(str(aluno[2].getEmail()))

        tel = aluguelcontrol.selecionarTel(aluno[0].getIdAluno())
        self.tellAlunoTabela0.set_text(str(tel.telefone.getNumAluno()))
        self.tellPaisTabela0.set_text(str(tel.telefone.getNumResponsavel()))

        tel = aluguelcontrol.selecionarTel(aluno[1].getIdAluno())
        self.tellAlunoTabela1.set_text(str(tel.telefone.getNumAluno()))
        self.tellPaisTabela1.set_text(str(tel.telefone.getNumResponsavel()))

        tel = aluguelcontrol.selecionarTel(aluno[2].getIdAluno())
        self.tellAlunoTabela2.set_text(str(tel.telefone.getNumAluno()))
        self.tellPaisTabela2.set_text(str(tel.telefone.getNumResponsavel()))

        print(aluno[0].getIdLivro())
        print(aluno[1].getIdLivro())
        print(aluno[2].getIdLivro())

        livros = aluguelcontrol.selecionarUmLivro(aluno[0].getIdLivro())
        self.tituloTabela0.set_text(livros.livro.getNomeLivro())
        self.autorTabela0.set_text(str(livros.livro.getAutor()))
        self.editoraTabela0.set_text(str(livros.livro.getEditora()))

        livros = aluguelcontrol.selecionarUmLivro(aluno[1].getIdLivro())
        self.tituloTabela1.set_text(livros.livro.getNomeLivro())
        self.autorTabela1.set_text(str(livros.livro.getAutor()))
        self.editoraTabela1.set_text(str(livros.livro.getEditora()))

        livros = aluguelcontrol.selecionarUmLivro(aluno[2].getIdLivro())
        self.tituloTabela2.set_text(livros.livro.getNomeLivro())
        self.autorTabela2.set_text(str(livros.livro.getAutor()))
        self.editoraTabela2.set_text(str(livros.livro.getEditora()))

        data = aluguelcontrol.selecionarData() 
        self.dataEmissaoTabela0.set_text(str(data[0].getDataInicial()))
        self.dataEntregaTabela0.set_text(str(data[0].getDataFinal()))
        self.dataEmissaoTabela1.set_text(str(data[1].getDataInicial()))
        self.dataEntregaTabela1.set_text(str(data[1].getDataFinal())) 
        self.dataEmissaoTabela2.set_text(str(data[2].getDataInicial()))
        self.dataEntregaTabela2.set_text(str(data[2].getDataFinal()))

        self.tabela.show_all()
        self.tabela.connect("destroy",Gtk.main_quit)
        Gtk.main()

        #metodo da estilizacao da tela, css  
    def css(self):
        css = b"""
        .tlTabela{
            background-color: #9ab6c8;
            
        }
        .botoes{
            border-radius: 50px;
        }
        
         .tlAlerta{
            background-color: #fff;
            
        }
        .tlRenovacao{
            background-color: #9ab6c8;
        
        }
        .gridDataNova{
            background-color: #fff;
            border-radius: 50px;
            
        }
        
        """
        style_provider = Gtk.CssProvider() #instanciando css provider
        style_provider.load_from_data(css) 
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            )
        
       
    #metodo do botao devolvido, deleta os dados do banco, da primeira linha.
    def devolvido(self,evt):
        aluguelcontrol = AluguelControle()
        aluguelid = aluguelcontrol.selecionarIdsAlugueis()
        dataid = aluguelcontrol.selecionarData()
        aluguelcontrol.removerData(dataid[0].getIdData())
        aluguelcontrol.removerAluguel(aluguelid[0].getIdAluguel())
        aluguelcontrol.removerAluno(aluguelid[0].getIdAluno())
        aluguelcontrol.removerLivro(aluguelid[0].getIdLivro())
        print("apagado com sucesso")


    #Ao clicar em devolvido obotao ira realizar a acao de mudar o conteudo da tabela para ""(vazio)
    
        self.serieTabela0.set_text("")
        self.cursoTabela0.set_text("")
        self.nomeTabela0.set_text("")
        self.emailTabela0.set_text("")
        self.tellAlunoTabela0.set_text("")
        self.tellPaisTabela0.set_text("")
        self.tituloTabela0.set_text("")
        self.autorTabela0.set_text("")
        self.editoraTabela0.set_text("")
        self.dataEmissaoTabela0.set_text("")
        self.dataEntregaTabela0.set_text("")
    
    #metodo do botao devolvido, deleta os dados do banco, da segunda linha.
    def devolvido1(self,evt):
        aluguelcontrol = AluguelControle()
        aluguelid = aluguelcontrol.selecionarIdsAlugueis()
        dataid = aluguelcontrol.selecionarData()
        aluguelcontrol.removerData(dataid[1].getIdData())
        aluguelcontrol.removerAluguel(aluguelid[1].getIdAluguel())
        aluguelcontrol.removerAluno(aluguelid[1].getIdAluno())
        aluguelcontrol.removerLivro(aluguelid[1].getIdLivro())
        print("apagado com sucesso") 
    #Ao clicar em devolvido obotao ira realizar a acao de mudar o conteudo da tabela para ""(vazio)
    
        self.serieTabela1.set_text("")
        self.cursoTabela1.set_text("")
        self.nomeTabela1.set_text("")
        self.emailTabela1.set_text("")
        self.tellAlunoTabela1.set_text("")
        self.tellPaisTabela1.set_text("")
        self.tituloTabela1.set_text("")
        self.autorTabela1.set_text("")
        self.editoraTabela1.set_text("")
        self.dataEmissaoTabela1.set_text("")
        self.dataEntregaTabela1.set_text("")

    #metodo do botao devolvido, deleta os dados do banco, da terceira linha.
    def devolvido2(self,evt):
        aluguelcontrol = AluguelControle()
        aluguelid = aluguelcontrol.selecionarIdsAlugueis()
        dataid = aluguelcontrol.selecionarData()
        aluguelcontrol.removerData(dataid[2].getIdData())
        aluguelcontrol.removerAluguel(aluguelid[2].getIdAluguel())
        aluguelcontrol.removerAluno(aluguelid[2].getIdAluno())
        aluguelcontrol.removerLivro(aluguelid[2].getIdLivro())
        print("apagado com sucesso")
    #Ao clicar em devolvido obotao ira realizar a acao de mudar o conteudo da tabela para ""(vazio)
    
        self.serieTabela2.set_text("")
        self.cursoTabela2.set_text("")
        self.nomeTabela2.set_text("")
        self.emailTabela2.set_text("")
        self.tellAlunoTabela2.set_text("")
        self.tellPaisTabela2.set_text("")
        self.tituloTabela2.set_text("")
        self.autorTabela2.set_text("")
        self.editoraTabela2.set_text("")
        self.dataEmissaoTabela2.set_text("")
        self.dataEntregaTabela2.set_text("")

    #metodo do botao sair.
    def sair (self,evt):
        self.tabela.destroy()
        #sair da aplicacao
        
    #metodo do botao renovacao, abre a tela de mudanca da data de entrega
    def renovacao(self,evt):
        builder = Gtk.Builder()
        builder.add_from_file("visual/Renovar.glade")
        self.renovar = builder.get_object("tlRenovacao")
        self.dataNova = builder.get_object("tfDataNova")
        builder.connect_signals(self)
        self.renovar.show_all()
        self.renovar.connect("destroy",Gtk.main_quit)
        Gtk.main()
    
    def renovacao1(self,evt):
        builder = Gtk.Builder()
        builder.add_from_file("visual/renovacao2.glade")
        self.renovar1 = builder.get_object("tlRenovacao1")
        self.dataNova1 = builder.get_object("tfDataNova1")
        builder.connect_signals(self)
        self.renovar1.show_all()
        self.renovar1.connect("destroy",Gtk.main_quit)
        Gtk.main()

    def renovacao2(self,evt):
        builder = Gtk.Builder()
        builder.add_from_file("visual/renovacao3.glade")
        self.renovar2 = builder.get_object("tlRenovacao2")
        self.dataNova2 = builder.get_object("tfDataNova2")
        builder.connect_signals(self)
        self.renovar2.show_all()
        self.renovar2.connect("destroy",Gtk.main_quit)
        Gtk.main()

    #metodo do botao voltar da tela de renovar a data de Entrega
    def voltar (self, evt):
        self.renovar.destroy()
    
    def voltarHome (self, evt):

        self.tabela.hide()
        self.tabela.destroy()
        self.alu = AluguelLivro()    
            
    #metodo do botao enviar que envia o novo valor da data de Entrega
    def enviarRenovacao(self, evt):
        aluguelcontrole = AluguelControle()
        if aluguelcontrole.verificarCampoRenovacao(self.dataNova.get_text()):
            #label de entrega data serao modificado com a nova data
            self.dataEntregaTabela0.set_text(self.dataNova.get_text())
            data = aluguelcontrole.selecionarData()
            aluguelcontrole.updateData(self.dataNova.get_text(), data[0].getDataFinal(), data[0].getIdData())
            self.renovar.destroy()
        #caso os campos de entrada estejam vazios
        else:
            #abre-se a tela de alerta
            builder = Gtk.Builder()
            builder.add_from_file("visual/Alerta1.glade")
            self.alert = builder.get_object("tlAlerta1")
            builder.connect_signals(self)  
            self.alert.show_all() 
            self.alert.connect("destroy",Gtk.main_quit)
            Gtk.main()

    def enviarRenovacao1(self, evt):
        aluguelcontrole = AluguelControle()
        if aluguelcontrole.verificarCampoRenovacao(self.dataNova1.get_text()):
            #label de entrega data serao modificado com a nova data
            self.dataEntregaTabela1.set_text(self.dataNova1.get_text())
            data = aluguelcontrole.selecionarData()
            aluguelcontrole.updateData(self.dataNova1.get_text(), data[1].getDataFinal(), data[1].getIdData())
            self.renovar1.destroy()
        #caso os campos de entrada estejam vazios
        else:
            #abre-se a tela de alerta
            builder = Gtk.Builder()
            builder.add_from_file("visual/Alerta1.glade")
            self.alert = builder.get_object("tlAlerta1")
            builder.connect_signals(self)  
            self.alert.show_all() 
            self.alert.connect("destroy",Gtk.main_quit)
            Gtk.main()

    def enviarRenovacao2(self, evt):
        aluguelcontrole = AluguelControle()
        if aluguelcontrole.verificarCampoRenovacao(self.dataNova2.get_text()):
            #label de entrega data serao modificado com a nova data
            self.dataEntregaTabela2.set_text(self.dataNova2.get_text())
            data = aluguelcontrole.selecionarData()
            aluguelcontrole.updateData(self.dataNova2.get_text(), data[2].getDataFinal(), data[2].getIdData())
            self.renovar2.destroy()
        #caso os campos de entrada estejam vazios
        else:
            #abre-se a tela de alerta
            builder = Gtk.Builder()
            builder.add_from_file("visual/Alerta1.glade")
            self.alert = builder.get_object("tlAlerta1")
            builder.connect_signals(self)  
            self.alert.show_all() 
            self.alert.connect("destroy",Gtk.main_quit)
            Gtk.main()
