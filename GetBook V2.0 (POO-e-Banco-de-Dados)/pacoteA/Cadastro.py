# -*- coding: utf-8 -*-
'''
Feito em 2019.2
@description: Classe (modelo Cadastro) de cadastro do usuario
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["BibliotecaPy - ", "Academico: Carliane, Waycon e Kailane"]
__version__="2.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__ = "14/12/2019"


class Cadastro:
    #atributos privados
    __iduser = 0
    __user = ""
    __email = ""
    __senha = "" 
    __confirm = ""
    __funcao = ""

    def setIduser(self, i):
        self.__iduser = i

    def getIduser(self):
        return self.__iduser

    def setUser(self, u):
        self.__user = u

    def getUser(self):
        return self.__user

    def setEmail(self, e):
        self.__email = e

    def getEmail(self):
        return self.__email    
    #metodo utilizado para modificar o valor da senha de Cadastro
    #paramentro: senha 
    def setSenha(self, s):
        self.__senha = s
    #metodo utilizado para mostrar o valor da senha de cadastro
    def getSenha(self):
        return self.__senha
    
    def setConf(self, c):
        self.__confirm = c

    def getConf(self):
        return self.__confirm
    
    def setFuncao(self, f):
        self.__funcao = f

    def getFuncao(self):
        return self.__funcao
