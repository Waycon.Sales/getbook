# -*- coding: utf-8 -*-
'''

Feito em 2019.2
@description: Classe (modelo Login).
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["BibliotecaPy - ", "Academicos: Carliane, Waycon e Kailane"]
__version__="2.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__ = "14/12/2019"

class Login ():
    __userLogin=""
    __senhaLogin="" #senha de login privada
    
    def setUserLogin(self, ul):
        self.__userLogin = ul
    def getUserLogin(self):
        return self.__userLogin

    #metodo utilizado para modificar o valor da senha de login
    #paramentro: senha de login
    def setSenhaLogin(self, sl):
        self.__senhaLogin = sl
    #metodo utilizado para mostrar o valor da senha de login
    def getSenhaLogin(self):
        return self.__senhaLogin
