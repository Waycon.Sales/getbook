# -*- coding: utf-8 -*-
'''

Feito em 2019.2
@description: Classe (modelo Livro) Esta classe faz composicao com a classe Aluguel.
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["BibliotecaPy - ", "Academicos: Carliane, Waycon e Kailane"]
__version__="2.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__ = "15/12/2019"

class Livro:
    __nomeLivro=""
    __autor=""
    __editora=""
    
 
 		
    def setNomeLivro(self, nl):
        self.__nomeLivro = nl
      
    def getNomeLivro(self):
        return self.__nomeLivro       

    def setAutor(self,a):
        self.__autor = a
      
    def getAutor(self):
        return self.__autor       

    def setEditora(self,e):
        self.__editora = e
      
    def getEditora(self):
        return self.__editora

