# -*- coding: utf-8 -*-
'''

Feito em 2019.2
@description: Classe (modelo Telefone) que contem objetos como o telefone dos pais e telefone do aluno. Compoe Aluguel (modelo)
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["BibliotecaPy - ", "Academicos: Carliane, Waycon e Kailane"]
__version__="2.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__ = "15/12/2019"

class Telefone:
    __numeroAluno=""
    __numeroResponsavel=""
    

    def setNumAluno(self,na):
        self.__numeroAluno = na
      
    def getNumAluno(self):
        return self.__numeroAluno

    def setNumResponsavel(self,nr):
        self.__numeroResponsavel = nr
      
    def getNumResponsavel(self):
        return self.__numeroResponsavel
        
