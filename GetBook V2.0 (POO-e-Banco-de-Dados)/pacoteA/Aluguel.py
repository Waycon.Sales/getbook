# -*- coding: utf-8 -*-
'''

Feito em 2019.2
@description: Classe (modelo Aluguel) que representa o processo de aluguel de livros. Herda tudo de aluno (modelo), composta por Livro (modelo) e Telefone (modelo)
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["BibliotecaPy - ", "Academicos: Carliane, Waycon e Kailane"]
__version__="2.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__="15/12/2019"

from pacoteA.Aluno import  Aluno
from pacoteA.Livro import Livro
from pacoteA.Telefone import Telefone


class Aluguel(Aluno):
    __idLivro= 0
    __idAluguel = 0 
    __idUser = 0
    __idData = 0
    __dataInicial=""
    __dataFinal=""  #data de entrega privada
    livro = Livro() #instancia de livro
    telefone = Telefone() #instancia de telefone
        
        # Construtor da superClasse na subClasse
        #parametro nome, curso
    def __init__(self,n,c):
        super().__init__(n,c)

        #encapsulamentos
    def setDataFinal(self,df):
        self.__dataFinal = df
    def getDataFinal(self):
        return self.__dataFinal
        
    def setDataInicial(self,di):
        self.__dataInicial = di
    def getDataInicial(self):
        return self.__dataInicial
            
    def setIdUser(self,iu):
        self.__idUser = iu
    def getIdUser(self):
        return self.__idUser

    def setIdAluguel(self,ial):
        self.__idAluguel = ial
    def getIdAluguel(self):
        return self.__idAluguel

    def setIdData(self,ida):
        self.__idData = ida
    def getIdData(self):
        return self.__idData

    def setIdLivro(self,il):
        self.__idLivro = il
      
    def getIdLivro(self):
        return self.__idLivro
