# -*- coding: utf-8 -*-
'''

Feito em 2019.2
@description: Classe (modelo Aluno) abstrata que representa o aluno.
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["BibliotecaPy - ", "Academicos: Carliane, Waycon e Kailane"]
__version__="2.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__="15/12/2019"

from abc import ABC #para torna-la abstrata
class Aluno (ABC):
    __idAluno = 0
    __nome=""
    __curso=""
    __serie=""
    __email="" 

    def setIdAluno(self, ia):
        self.__idAluno = ia
      
    def getIdAluno(self):
        return self.__idAluno

    def setNome(self, n):
        self.__nome = n
      
    def getNome(self):
        return self.__nome

    def setCurso(self, c):
        self.__curso = c
      
    def getCurso(self):
        return self.__curso       

    def setSerie(self, s):
        self.__serie = s
      
    def getSerie(self):
        return self.__serie

    def setEmail(self, e):
        self.__email = e
      
    def getEmail(self):
        return self.__email
    #Construtor que modifica o comportamento dos atributos nome e curso 
    #parametro: nome , curso
    def __init__ (self, n, c):
        self.setNome(n) 
        self.setCurso(c)

    
    
    
   
        
