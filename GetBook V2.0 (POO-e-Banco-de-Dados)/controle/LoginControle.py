# -*- coding: utf-8 -*-
'''
Feito em 2019
@description: Classe (Controle) de Login
@author: Waycon Sales
'''
__author__="Waycon Sales"
__credits__=["BibliotecaPy - ", "Academicos: Carliane, Waycon e Kailane"]
__version__="2.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__ = "14/12/2019"

from controle.Conexao import Conexao
from pacoteA.Login import Login

class LoginControle ():
    
    def selecionarUser(self, login):
            Logado = "" #criando a variavel que sera retornada com o id
            Falha = "usuario nÃ£o encontrado"
            try:
                user = login.getUserLogin() #atribuindo dados da modelo a variavel para fazer uma busca na tabela usuarios
                con = Conexao() #abrindo a conexao
                sql = "SELECT id FROM usuarios WHERE nomeUser = %s;" #preparando o comando, ele selecionara nomente o id que corresponder ao nome passado pelo usuario 
                valor = (user,) #atribuindo a valor a variavel user que subtituira a variavel de referencia
                cursor = con.getCon().cursor(dictionary=True) #deixa os dados organizados como um dicionario
                cursor.execute(sql,valor)
                consulta = cursor.fetchone() 
                 #executando a sql
                #consulta = cursor.fetchone() #retornando um unico registro
                Logado = consulta['id'] #atribuindo a variavel Logado o retorno da consulta
                return Logado
            except Exception as e:
                print("Erro:",str(e))
                return Falha
            
    def selecionarSenha(self, login, i):
            SenhaId = "" #criando a variavel que sera retornada com o id
            Falha = "senha nÃ£o encontrada"
            try:
                senha = login.getSenhaLogin() #atribuindo dados da modelo a variavel para fazer uma busca na tabela usuarios
                iduser = i
                con = Conexao() #abrindo a conexao
                sql = "SELECT id FROM senhas WHERE senhaAtual = %s and senhaUser = %s;" #preparando o comando, ele selecionara nomente o id que corresponder ao nome passado pelo usuario 
                valor = (senha,iduser,) #atribuindo a valor a variavel senha que subtituira a variavel de referencia
                cursor = con.getCon().cursor(dictionary=True) #deixa os dados organizados como um dicionario
                cursor.execute(sql,valor)
                consulta = cursor.fetchone() 
                 #executando a sql
                #consulta = cursor.fetchone() #retornando um unico registro
                SenhaId = consulta['id'] #atribuindo a variavel SehaId o retorno da consulta
                return SenhaId
            except Exception as e:
                print("Erro:",str(e))
                return Falha
    
    







    #metodo para verificar se os campos estao preenchidos
    #paramentro: senha e usuario
    def verificarCampo(self, sen, usr):
        # se senha e usuario forem diferentes de ""(vazio), retorna True
        if sen and usr != "" : 
            return True
        #se nao retorna False
        else:
            return False
