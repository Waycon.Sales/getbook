import mysql.connector
class Conexao:
    __con = ""

    def getCon(self):
        return self.__con
    def setCon(self,data):
        self.__con = data
    def __init__(self):
        try:
            usr="root"
            banco="biblioteca"
            senha="123456"
            hst="localhost"
            #executar biblioteca de conexao
            self.setCon(mysql.connector.connect(user=usr, password=senha, database=banco,host=hst))
        except mysql.connector.Error as e :
            print("Erro ao conectar com o banco ", e)
        except Exception as e :
            print("Erro geral ", e)
    def fecharConexao(self):
        self.__con = None 

