# -*- coding: utf-8 -*-
'''
Feito em 2019.2
@description: Classe (Controle) de Cadastro
@author: Waycon
'''
__author__="Waycon Sales"
__credits__=["BibliotecaPy - ", "Academicos: Carliane, Waycon e Kailane"]
__version__="2.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__ = "14/12/2019"

from controle.Conexao import Conexao
from pacoteA.Cadastro import Cadastro

class ControleCadastro:
    def inserirUser(self,Cadastro):
        try:
                con = Conexao() #abrindo a conexao com o bd 
                user = Cadastro.getUser() #atribuindo dados da modelo a variaveis para inserir na tabela usuarios
                funcao = Cadastro.getFuncao()
                sql = "INSERT INTO usuarios(nomeUser,funcao) VALUES(%s,%s);" #variavel que recebe um comando do banco de dados com valores sendo variaveis de referencia        
                cursor  = con.getCon().cursor() #metodo utilizado para para o bd preparando para alguma alteraÃ§Ã£o ou consulta no banco
                vals = (user,funcao) #vals recebe os valores que estÃ£o nas variaveis user e funcao
                cursor.execute(sql,vals) #executa o comando sql                                                 
                con.getCon().commit() #commit envia os dados
                con.fecharConexao() #finalizando a conexao com o banco de dados 
                return True 
        except Exception as e:
            print("Erro geral:",str(e))
            return False

    def inserirEmail(self,Cadastro):
        try:
                con = Conexao()
                iduser = Cadastro.getIduser()
                email = Cadastro.getEmail()
                sql = "INSERT INTO email(email,emailUser) VALUES(%s,%s);"         
                cursor  = con.getCon().cursor()
                vals = (email,iduser)
                cursor.execute(sql,vals)
                con.getCon().commit()                
                con.fecharConexao()
                return True
        except Exception as e:
            print("Erro geral:",str(e))
            return False

    def inserirSenha(self,Cadastro):
        try:
                con = Conexao()
                iduser = Cadastro.getIduser()
                senha = Cadastro.getSenha()
                sql = "INSERT INTO senhas(senhaAtual,senhaUser) VALUES(%s,%s);"  
                cursor  = con.getCon().cursor()
                vals = (senha,iduser)
                cursor.execute(sql,vals)
                con.getCon().commit()                
                con.fecharConexao()
                return True
        except Exception as e:
            print("Erro geral:",str(e))
            return False


    def removerUser(self,cadastro):
        try:
            con = Conexao() #abrindo a conexao com o bd
            id = cadastro.getIduser() #passando dados da modelo para a variavel id que sera  usada no comando sql
            cursor = con.getCon().cursor() #metodo utilizado para para o bd preparando-o para alguma alteraÃ§Ã£o ou consulta no banco
            sql = "DELETE FROM usuarios WHERE id = %s;" #comando sql para deletar usuario do banco de dados
            valor = (id,) #variavel valor recebendo id que sera colocada no local da variavel de referencia na sql
            cursor.execute(sql,valor) #executa o comando da sql no bd
            con.getCon().commit() #envia os dados
            con.fecharConexao() #finaliza a conecxao com o banco
            return True
        except Exception as e:
            print("Erro geral:",e)
            return False

    def removerSenha(self,cadastro):
        try:
            con = Conexao() #abrindo a conexao com o bd
            id = cadastro.getIduser() #passando dados da modelo para a variavel id que sera  usada no comando sql
            cursor = con.getCon().cursor() #metodo utilizado para para o bd preparando-o para alguma alteraÃ§Ã£o ou consulta no banco
            sql = "DELETE FROM senhas WHERE senhaUser = %s;" #comando sql para deletar a senha do usuario do banco de dados
            valor = (id,) #variavel valor recebendo id que sera colocada no local da variavel de referencia na sql
            cursor.execute(sql,valor) #executa o comando da sql no bd
            con.getCon().commit() #envia os dados
            con.fecharConexao() #finaliza a conecxao com o banco
            return True
        except Exception as e:
            print("Erro geral:",e)
            return False

    def selecionarId(self):
            dados = "" #criando a variavel que sera retornada com o id
            try:
                con = Conexao() #abrindo a conecxao com o banco de dados
                sql = "SELECT id FROM usuarios ORDER BY id DESC LIMIT 1;" #comando para selecionar ultimo id do banco de dados 
                cursor = con.getCon().cursor(dictionary=True) #deixa os dados organizados como um dicionario
                cursor.execute(sql) #executando a sql
                consulta = cursor.fetchone() #retornando um unico registro
                dados = consulta['id'] #atribuindo a variavel dados o retorno da consulta
                con.fecharConexao() 
            except Exception as e:
                print("Erro:",str(e))
            return dados
    
    def selecionarUm(self, cadastro):
            dados = "" #criando a variavel que sera retornada com o id
            try:
                user = cadastro.getUser() #atribuindo dados da modelo a variavel para fazer uma busca na tabela usuarios
                con = Conexao() #abrindo a conexao
                sql = "SELECT id FROM usuarios WHERE nomeUser = %s;" #preparando o comando, ele selecionara nomente o id que corresponder ao nome passado pelo usuario 
                valor = (user,) #atribuindo a valor a variavel user que subtituira a variavel de referencia
                cursor = con.getCon().cursor(dictionary=True) #deixa os dados organizados como um dicionario
                cursor.execute(sql,valor) #executando a sql
                consulta = cursor.fetchone() #retornando um unico registro
                dados = consulta['id'] #atribuindo a variavel dados o retorno da consulta
                con.fecharConexao()
            except Exception as e:
                print("Erro:",str(e))
            return dados


    #metodo para verificar se as senha e igual a confirmacao
    #parametros: senha, confirmacao 
    def verificarSenha(self,senha,confirmacao):
        #se as senhas forem iguais retorna True
        if senha == confirmacao:
            return True
        #se nao retorna False
        else:
            return False
        

    #metodo para verificar se os campos estaao preenchidos
    #paramentro: usario, senha, confirmacao e email    
    def verificarCampos(self,usr,sen,conf,email):
        # se senha e usuario forem diferentes de ""(vazio), retorna True
        if usr and sen and conf and email != "":
            return True
        #se nao retorna False
        else:
            return False
            
