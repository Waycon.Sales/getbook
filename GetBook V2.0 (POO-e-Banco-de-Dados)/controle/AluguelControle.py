# -*- coding: utf-8 -*-
'''
Feito em 2019.2
@description: Classe (Controle) de Aluguel.
@author: Waycon Sales
'''
__author__="Waycon Sales"
__credits__=["BibliotecaPy - ", "Academicos: Carliane, Waycon e Kailane"]
__version__="2.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__ = "15/12/2019"

from controle.Conexao import Conexao
from pacoteA.Aluguel import Aluguel

class AluguelControle :
    def inserirLivro(self,aluguel):
        try:
            con = Conexao()
            nome = aluguel.livro.getNomeLivro()
            autor = aluguel.livro.getAutor()
            editora = aluguel.livro.getEditora()            
            sql = "INSERT INTO livros(nomeLivro,autor,editora) VALUES(%s,%s,%s);"         
            cursor  = con.getCon().cursor()
            vals = (nome,autor,editora,)
            cursor.execute(sql,vals)
            con.getCon().commit()                
            con.fecharConexao()
            return True
        except Exception as e:
            print("Erro geral:",str(e))
            return False

    def selecionarIdUser(self,user):
        dados = "" #criando a variavel que sera retornada com o id
        try:
            con = Conexao() #abrindo a conexao
            sql = "SELECT id FROM usuarios WHERE nomeUser = %s;" #preparando o comando, ele selecionara nomente o id que corresponder ao nome passado pelo usuario 
            valor = (user,) #atribuindo a valor a variavel user que subtituira a variavel de referencia
            cursor = con.getCon().cursor(dictionary=True) #deixa os dados organizados como um dicionario
            cursor.execute(sql,valor) #executando a sql
            consulta = cursor.fetchone() #retornando um unico registro
            dados = consulta['id'] #atribuindo a variavel dados o retorno da consulta
            con.fecharConexao()
        except Exception as e:
            print("Erro:",str(e))
        return dados

    def inserirAluno(self,aluguel):
        try:  
            con = Conexao() #abrindo a conexao com o bd 
            nome = aluguel.getNome() #atribuindo dados da modelo a variaveis para inserir na tabela usuarios
            serie = aluguel.getSerie()
            curso = aluguel.getCurso()
            email = aluguel.getEmail()
            telAluno = aluguel.telefone.getNumAluno()
            telResp = aluguel.telefone.getNumResponsavel()
            user = aluguel.getIdUser()
            livro = aluguel.getIdLivro()
            sql = "INSERT INTO alunos(nomeAluno,serie,curso,email,telAluno,telPais,user,livro) VALUES(%s,%s,%s,%s,%s,%s,%s,%s);" #variavel que recebe um comando do banco de dados com valores sendo variaveis de referencia        
            cursor  = con.getCon().cursor() #metodo utilizado para para o bd preparando para alguma alteraÃ§Ã£o ou consulta no banco
            vals = (nome,serie,curso,email,telAluno,telResp,user,livro,) #vals recebe os valores que estÃ£o nas variaveis user e funcao
            cursor.execute(sql,vals) #executa o comando sql                                                 
            con.getCon().commit() #commit envia os dados
            con.fecharConexao() #finalizando a conexao com o banco de dados 
            return True 
        except Exception as e:
            print("Erro geral:",str(e))
            return False

    def selecionarIdAluno(self, aluguel):
        dados = "" #criando a variavel que sera retornada com o id
        try:
            nome = aluguel.getNome()
            con = Conexao() #abrindo a conexao
            sql = "SELECT id FROM alunos WHERE nomeAluno = %s;" #preparando o comando, ele selecionara somente o id que corresponder ao nome passado pelo usuario 
            valor = (nome,) #atribuindo a valor a variavel nome que subtituira a variavel de referencia
            cursor = con.getCon().cursor(dictionary=True) #deixa os dados organizados como um dicionario
            cursor.execute(sql,valor) #executando a sql
            consulta = cursor.fetchone() #retornando um unico registro
            dados = consulta['id'] #atribuindo a variavel dados o retorno da consulta
            con.fecharConexao()
        except Exception as e:
            print("Erro:",str(e))
        return dados

    #insere os dados na tabela aluguel, que contÃ©n os ids de aluno, user e livro que Ã© o relacionamento ternario da aplicacao
    def inserirAluguel(self,aluguel):
        try:
            con = Conexao()
            aluno = aluguel.getIdAluno()
            user = aluguel.getIdUser()
            livro = aluguel.getIdLivro()
            sql = "INSERT INTO alugueis(alunoAl,userAl,livroAl) VALUES(%s,%s,%s);"         
            cursor  = con.getCon().cursor()
            vals = (aluno,user,livro,)
            cursor.execute(sql,vals)
            con.getCon().commit()                
            con.fecharConexao()
            return True
        except Exception as e:
            print("Erro geral:",str(e))
            return False

    #paga o id apenas de alguel
    def selecionarIdAluguel(self, aluguel):
        dados = "" #criando a variavel que sera retornada com o id
        try:
            aluno = aluguel.getIdAluno()
            con = Conexao() #abrindo a conexao
            sql = "SELECT id FROM alugueis WHERE alunoAl = %s;" #preparando o comando, ele selecionara somente o id que corresponder ao nome passado pelo usuario 
            valor = (aluno,) #atribuindo a valor, a variavel livro que subtituira a variavel de referencia
            cursor = con.getCon().cursor(dictionary=True) #deixa os dados organizados como um dicionario
            cursor.execute(sql,valor) #executando a sql
            consulta = cursor.fetchone() #retornando um unico registro
            dados = consulta['id'] #atribuindo a variavel dados o retorno da consulta
            con.fecharConexao()
        except Exception as e:
            print("Erro:",str(e))
        return dados

    #selecionar o id dos livros
    def selecionarIdLivro(self, aluguel):
        dados = "" #criando a variavel que sera retornada com o id
        try:
            nomeLivro = aluguel.livro.getNomeLivro()
            con = Conexao() #abrindo a conexao
            sql = "SELECT id FROM livros WHERE nomeLivro = %s;" #preparando o comando, ele selecionara somente o id que corresponder ao nome passado pelo usuario 
            valor = (nomeLivro,) #atribuindo a valor, a variavel livro que subtituira a variavel de referencia
            cursor = con.getCon().cursor(dictionary=True) #deixa os dados organizados como um dicionario
            cursor.execute(sql,valor) #executando a sql
            consulta = cursor.fetchone() #retornando um unico registro
            dados = consulta['id'] #atribuindo a variavel dados o retorno da consulta
            con.fecharConexao()
        except Exception as e:
            print("Erro:",str(e))
        return dados

    #metodo de inserir data, recebe a data inicial o id de aluguel e a data final
    def inserirData(self,aluguel):
        try:
            con = Conexao()
            dataInicial = aluguel.getDataInicial()
            dataFinal = aluguel.getDataFinal()
            aluguel = aluguel.getIdAluguel()
            sql = "INSERT INTO data(inicial,finalAtual,aluguel) VALUES(%s,%s,%s);"         
            cursor  = con.getCon().cursor()
            vals = (dataInicial,dataFinal,aluguel,)
            cursor.execute(sql,vals)
            con.getCon().commit()                
            con.fecharConexao()
            return True
        except Exception as e:
            print("Erro geral:",str(e))
            return False

    #metodo para selecionar os alunos
    def selecionarAlunos(self):
        lista = ""
        n = "nome"
        c = "curso"

        try:
            conexao = Conexao()
            sql = "SELECT * FROM alunos;"
            cursor = conexao.getCon().cursor(dictionary=True)
            lista =  []
            cursor.execute(sql)
            consulta = cursor.fetchall()
            for i in range(0, consulta.__len__(),1):
                print(consulta[i]['livro'])
                aluguel = Aluguel(n,c)
                aluguel.setIdAluno(consulta[i]['id'])
                aluguel.setIdLivro(consulta[i]['livro'])
                aluguel.setNome(consulta[i]['nomeAluno'])
                aluguel.setSerie(consulta[i]['serie'])
                aluguel.setCurso(consulta[i]['curso'])
                aluguel.setEmail(consulta[i]['email'])
                lista.append(aluguel)

        except Exception as e:
            print("Erro:" + str(e))
        return lista
        conexao.fecharConexao()

    '''def selecionarLivros(self):
        lista = ""
        n = "nome"
        c = "curso"
        try:
            conexao = Conexao()
            sql = "SELECT * FROM livros;"
            cursor = conexao.getCon().cursor(dictionary=True)
            lista =  []
            cursor.execute(sql)
            consulta = cursor.fetchall()
            for i in range(0, consulta.__len__(),1):
                aluguel = Aluguel(n,c)
                aluguel.livro.setNomeLivro(consulta[i]['nomeLivro'])
                aluguel.livro.setAutor(consulta[i]['autor'])
                aluguel.livro.setEditora(consulta[i]['editora'])
                lista.append(aluguel)
            conexao.fecharConexao()
        except Exception as e:
            print("Erro:" + str(e))
        return lista
        '''
    #metodo para selecionar apenas um livro
    def selecionarUmLivro(self,id):
        lista = ""
        n = "nome"
        c = "curso"
        aluguel = ""
        try:
            conexao = Conexao()
            sql = "SELECT * FROM livros WHERE id = %s;"
            cursor = conexao.getCon().cursor(dictionary=True)
            valor = (id,)
            cursor.execute(sql,valor)
            consulta = cursor.fetchone()
            aluguel = Aluguel(n,c)
            aluguel.livro.setNomeLivro(consulta['nomeLivro'])
            aluguel.livro.setAutor(consulta['autor'])
            aluguel.livro.setEditora(consulta['editora'])
        except Exception as e:
            print("Erro : " + str(e))
        return aluguel
        conexao.fecharConexao() 

    #selecionar telefone do aluno e pais
    def selecionarTel(self,id):
        lista = ""
        n = "nome"
        c = "curso"
        aluguel = ""
        try:
            conexao = Conexao()
            sql = "SELECT * FROM alunos WHERE id = %s;"
            cursor = conexao.getCon().cursor(dictionary=True)
            valor = (id,)
            cursor.execute(sql,valor)
            consulta = cursor.fetchone()
            aluguel = Aluguel(n,c)
            aluguel.telefone.setNumAluno(consulta['telAluno'])
            aluguel.telefone.setNumResponsavel(consulta['telPais'])
        except Exception as e:
            print("Erro : " + str(e))
        return aluguel
        conexao.fecharConexao()

    #metodo que seleciona todoas as datas e coloca em uma lista
    def selecionarData(self):
        lista = ""
        n = "nome"
        c = "curso"
        try:
            conexao = Conexao()
            sql = "SELECT * FROM data;"
            cursor = conexao.getCon().cursor(dictionary=True)
            lista =  []
            cursor.execute(sql)
            consulta = cursor.fetchall()
            for i in range(0, consulta.__len__(),1):
                aluguel = Aluguel(n,c)
                aluguel.setIdData(consulta[i]['id'])
                aluguel.setDataInicial(consulta[i]['inicial'])
                aluguel.setDataFinal(consulta[i]['finalAtual'])
                lista.append(aluguel)
        except Exception as e:
            print("Erro:" + str(e))
        return lista
        conexao.fecharConexao()

    def removerLivro(self,id):
        try:
            con = Conexao()
            cursor = con.getCon().cursor()
            sql = "DELETE FROM livros WHERE id = %s"
            valor = (id,)
            cursor.execute(sql,valor)
            con.getCon().commit()
            con.fecharConexao()
        except Exception as e:
                print("Erro geral:",e)

    def removerAluno(self,id):
        try:
            con = Conexao()
            idAluno = id
            cursor = con.getCon().cursor()
            sql = "DELETE FROM alunos WHERE id = %s"
            valor = (idAluno,)
            cursor.execute(sql,valor)
            con.getCon().commit()
            con.fecharConexao()
        except Exception as e:
                print("Erro geral:",e)

    def removerAluguel(self,id):
        try:
            con = Conexao()
            idAluguel = id
            cursor = con.getCon().cursor()
            sql = "DELETE FROM alugueis WHERE id = %s"
            valor = (idAluguel,)
            cursor.execute(sql,valor)
            con.getCon().commit()
            con.fecharConexao()
        except Exception as e:
            print("Erro geral:",e)

    def removerData(self,id):
        try:
            con = Conexao()
            cursor = con.getCon().cursor()
            sql = "DELETE FROM data WHERE id = %s"
            valor = (id,)
            cursor.execute(sql,valor)
            con.getCon().commit()
            con.fecharConexao()
        except Exception as e:
            print("Erro geral:",e)

    #metodo de update da data, parametros, nova data, data antiga e o id para modificar.
    def updateData(self,novadata,dataAntiga,id):
        try:
            con = Conexao()
            sql = "UPDATE data SET finalAtual = %s, finalp1 = %s WHERE id = %s;"
            valores = (novadata,dataAntiga, id,)
            cursor = con.getCon().cursor()
            cursor.execute(sql,valores)
            con.getCon().commit()
        except Exception as e:
            print("Erro geral", e)


    #metodo para selecionar os id's da tabela Aluguel
    def selecionarIdsAlugueis(self):
        lista = ""
        n = "nome"
        c = "curso"
        try:
            conexao = Conexao()
            sql = "SELECT * FROM alugueis;"
            cursor = conexao.getCon().cursor(dictionary=True)
            lista =  []
            cursor.execute(sql)
            consulta = cursor.fetchall()
            for i in range(0, consulta.__len__(),1):    
                aluguel = Aluguel(n,c)
                aluguel.setIdAluno(consulta[i]['alunoAl'])
                aluguel.setIdAluguel(consulta[i]['id'])
                aluguel.setIdLivro(consulta[i]['livroAl'])
                lista.append(aluguel)
        except Exception as e:
            print("Erro:" + str(e))
        return lista
        conexao.fecharConexao()
    

    #metodo para verificar se os campos estao preenchidos
    #paramentro: todos os campos de emprestimo
    def verificarCampos(self,n,s,c,e,ta,tp,t,a,ed,di,df):
        
        if n and s and c and e and ta and tp and t and a and ed and di and df != "":
            return True
        #se nao retorna False
        else:
            return False
    #metodo para verificar se o campo estao preenchido   
    def verificarCampoRenovacao(self, nd):
        #diferentes de ""(vazio), retorna True
        if nd != "":
            return True
        #se nao retorna False
        else:
            return False
        
