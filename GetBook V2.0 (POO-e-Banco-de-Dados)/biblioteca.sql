DROP DATABASE IF EXISTS biblioteca; 
CREATE DATABASE IF NOT EXISTS biblioteca; 


CREATE TABLE biblioteca.usuarios(
	id INT(3) NOT NULL AUTO_INCREMENT,
	nomeUser VARCHAR(60) NOT NULL,
	funcao VARCHAR(20) NOT NULL,
	PRIMARY KEY(id)
	);

CREATE TABLE biblioteca.livros(
	id INT(3) NOT NULL AUTO_INCREMENT,
	nomeLivro VARCHAR(100) NOT NULL,
	autor VARCHAR(30) NOT NULL,
	editora VARCHAR(30) NOT NULL,
	PRIMARY KEY(id)
	);

CREATE TABLE biblioteca.email(
	id INT(3) NOT NULL AUTO_INCREMENT,
	email VARCHAR(60) NOT NULL,
	emailUser INT(3) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(emailUser) REFERENCES usuarios(id)
 	);

CREATE TABLE biblioteca.alunos(
	id INT(3) NOT NULL AUTO_INCREMENT,
	nomeAluno VARCHAR(80) NOT NULL,
	serie VARCHAR(20) NOT NULL,
	curso VARCHAR(20) NOT NULL,
	email VARCHAR(60) NOT NULL,
	telAluno VARCHAR(15) NOT NULL,
	telPais VARCHAR(15) NOT NULL,
	user INT(3) NOT NULL,
	livro INT(3) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(livro) REFERENCES livros(id),
	FOREIGN KEY(user) REFERENCES usuarios(id)
 	);


CREATE TABLE biblioteca.senhas(
	id INT(3) NOT NULL AUTO_INCREMENT,
	senhaAtual VARCHAR(160) NOT NULL,
	senhaP1 VARCHAR(160) NULL,
	senhaUser INT(3) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(senhaUser) REFERENCES usuarios(id)
 	);


CREATE TABLE biblioteca.alugueis(
	id INT(3) NOT NULL AUTO_INCREMENT,
	alunoAl INT(3) NOT NULL,
	userAl INT(3) NOT NULL,
	livroAl INT(3) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(alunoAl) REFERENCES alunos(id),
	FOREIGN KEY(userAl) REFERENCES usuarios(id),
	FOREIGN KEY(livroAl) REFERENCES livros(id)
 	);
CREATE TABLE biblioteca.data(
	id INT(3) NOT NULL AUTO_INCREMENT,
	inicial VARCHAR(10) NOT NULL,
	finalAtual VARCHAR(10) NOT NULL,
	finalp1 VARCHAR(10) NULL,
	aluguel INT(3) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY(aluguel) REFERENCES alugueis(id)
	);
