# -*- coding: utf-8 -*-
'''
Feito em 2019.1
@description: Classe (Controle) de Aluguel.
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["GetBook ", "Academicos: Waycon, Carliane e Kailane"]
__version__="1.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__ = "07/06/2019"

class AluguelControle :
    
    #metodo para verificar se os campos estao preenchidos
    #paramentro: todos os campos de emprestimo
    def verificarCampos(self,n,s,c,e,ta,tp,t,a,ed,di,df):
        # se senha e usuario forem diferentes de ""(vazio), retorna True
        if n and s and c and e and ta and tp and t and a and ed and di and df != "":
            return True
        #se nao retorna False
        else:
            return False
    #metodo para verificar se o campo estao preenchido   
    def verificarCampoRenovacao(self, nd):
        # se senha e usuario forem diferentes de ""(vazio), retorna True
        if nd != "":
            return True
        #se nao retorna False
        else:
            return False
        
