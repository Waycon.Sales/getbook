# -*- coding: utf-8 -*-
'''
Feito em 2019.1
@description: Classe (Controle) de Cadastro
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["GetBook ", "Academicos: Waycon, Carliane e Kailane"]
__version__="1.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__ = "07/06/2019"

class ControleCadastro:
    #metodo para verificar se as senha e igual a confirmaÃ§Ã£o
    #parametros: senha, confirmacao 
    def verificarSenha(self,senha,confirmacao):
        #se as senhas forem iguais retorna True
        if senha == confirmacao:
            return True
        #se nao retorna False
        else:
            return False
        
    #metodo para verificar se os campos estaao preenchidos
    #paramentro: usario, senha, confirmacao e email    
    def verificarCampos(self,usr,sen,conf,email):
        # se senha e usuario forem diferentes de ""(vazio), retorna True
        if usr and sen and conf and email != "":
            return True
        #se nao retorna False
        else:
            return False
            
