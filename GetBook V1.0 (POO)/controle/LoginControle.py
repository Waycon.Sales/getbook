# -*- coding: utf-8 -*-
'''
Feito em 2019.1
@description: Classe (Controle) de Login
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["GetBook ", "Academicos: Waycon, Carliane e Kailane"]
__version__="1.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__ = "07/06/2019"

class LoginControle ():
    
    #metodo para verificar se os campos estao preenchidos
    #paramentro: senha e usuario
    def verificarCampo(self, sen, usr):
        # se senha e usuario forem diferentes de ""(vazio), retorna True
        if sen and usr != "" : 
            return True
        #se nao retorna False
        else:
            return False
