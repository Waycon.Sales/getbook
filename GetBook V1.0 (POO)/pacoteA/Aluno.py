# -*- coding: utf-8 -*-
'''

Feito em 2019.1
@description: Classe (modelo Aluno) abstrata que representa o aluno.
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["GetBook ", "Academicos: Waycon, Carliane e Kailane"]
__version__="1.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__="07/06/2019"

from abc import ABC #para torna-la abstrata
class Aluno (ABC):
    nome=""
    curso=""
    serie=""
    email="" 
    #Construtor que modifica o comportamento dos atributos nome e curso 
    #parametro: nome , curso
    def __init__ (self, n, c):
        self.nome = n
        self.curso = c

    
    
    
   
        
