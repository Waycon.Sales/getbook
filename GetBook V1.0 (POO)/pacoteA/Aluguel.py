# -*- coding: utf-8 -*-
'''

Feito em 2019.1
@description: Classe (modelo Aluguel) que representa o processo de aluguel de livros. Herda tudo de aluno (modelo), composta por Livro (modelo) e Telefone (modelo)
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["GetBook ", "Academicos: Waycon, Carliane e Kailane"]
__version__="1.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__="07/06/2019"

from pacoteA.Aluno import  Aluno
from pacoteA.Livro import Livro
from pacoteA.Telefone import Telefone


class Aluguel(Aluno): 
    
        dataInicial=""
        __dataFinal=""  #data de entrega privada
        livro = Livro() #instancia de livro
        telefone = Telefone() #instancia de telefone
        
        # Construtor da superClasse na subClasse
        #parametro nome, curso
        def __init__(self,n,c):
            super().__init__(n,c)
        #metodo utilizado para modificar o valor da dataFinal
        #paramentro: dataNova
        def setDataFinal(self,df):
            self.__dataFinal = df
        #metodo utilizado para mostrar o valor da dataFinal
        def getDataFinal(self):
            return self.__dataFinal
        
        
            
