# -*- coding: utf-8 -*-
'''
Feito em 2019.1
@description: Classe (modelo Cadastro) de cadastro do usuario
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["GetBook ", "Academicos: Waycon, Carliane e Kailane"]
__version__="1.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__ = "07/06/2019"


class Cadastro:
    usr=""
    email=""
    __senha="" #senha de cadastro privada
    confirm=""

    #metodo utilizado para modificar o valor da senha de Cadastro
    #paramentro: senha 
    def setSenha(self, s):
        self.__senha = s
    #metodo utilizado para mostrar o valor da senha de cadastro
    def getSenha(self):
        return self.__senha
    
    
