# -*- coding: utf-8 -*-
'''

Feito em 2019.1
@description: Classe (modelo Livro) Esta classe faz composicao com a classe Aluguel.
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["GetBook ", "Academicos: Waycon, Carliane e Kailane"]
__version__="1.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__ = "07/06/2019"

class Livro:
        
        nomeLivro=""
        autor=""
        editora=""
        
