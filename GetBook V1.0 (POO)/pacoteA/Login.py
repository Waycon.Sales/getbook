# -*- coding: utf-8 -*-
'''

Feito em 2019.1
@description: Classe (modelo Login).
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["GetBook ", "Academicos: Waycon, Carliane e Kailane"]
__version__="1.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__ = "07/06/2019"

class Login ():
    usrLogin=""
    __senhaLogin="" #senha de login privada
    
    #metodo utilizado para modificar o valor da senha de login
    #paramentro: senha de login
    def setSenhaLogin(self, sl):
        self.__senhaLogin = sl
    #metodo utilizado para mostrar o valor da senha de login
    def getSenhaLogin(self):
        return self.__senhaLogin
