# -*- coding: utf-8 -*-
'''
feito em 2019.1
@description: Classe (visual)  da Tela de login do usuario, ao fazer o login ela abre o home principal
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["GetBook ", "Academicos: Waycon, Carliane e Kailane"]
__version__="1.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__="07/06/2019"

import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk,Gdk
from pacoteA.Login import Login
#classes importadas para a navegacao entre as telas
from controle.LoginControle import LoginControle
from visual.Homeprincipal import Homeprincipal
from visual.CadastroTela import CadastroTela


class LogarTela(): 
    #Construtor
    #Instanciacao da tela de login, da gtk builder...
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("visual/Login.glade")
        #object's da tela de login
        self.login = builder.get_object("tlLogin")
        self.userLog = builder.get_object("tfUsrLogin")
        self.senhaLog = builder.get_object("psSenhaLogin")
        self.css() #execucao do metodo  css
        builder.connect_signals(self)#pega os sinais dos botoees
        self.login.show_all()
        self.login.connect("destroy",Gtk.main_quit)
        Gtk.main()
        
    #metodo da estilizacao da tela, css     
    def css(self):
        css = b"""
        .tlLogin{
            background-color: #9ab6c8;
            
        }
        .grid{
            border-radius: 50px;
            background-color: #fff;
        }
        .btHome{
            border-radius: 50px;
        }
        .btEntrar{
            border-radius:50px;
        }
        .tlAlerta{
            background-color: #fff;
            
        }
        
        """
        style_provider = Gtk.CssProvider() #instanciando css provider
        style_provider.load_from_data(css) 
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
    
    #metodo do botao de cadastro que realiza o fechamento da tela de login e abre a de cadastro
    def cadastro (self, evt):
        self.login.hide()    
        self.login.destroy()
        self.cad = CadastroTela()
    #metodo do botao OK da tela de alerta, que fecha a tela de alerta         
    def Ok (self, evt):
        self.alert.destroy()
        
    def entrar (self, evt):
        #instancia das classes de Login (modelo) e LoginControle 
        con = LoginControle()
        log = Login()
        #como o uso de banco de dados nao esta disponivel, a unica condicao que se avalia e
        # se os campos de entrada estao preenchidos.
        #parametros: senha e usr 
        if con.verificarCampo(self.senhaLog.get_text(), self.userLog.get_text()):
            try:
                #utilizacao da instancia para preenchimento dos atributos da classe modelo de login
                log.usrLogin=(self.userLog.get_text())
                log.setSenhaLogin(self.senhaLog.get_text())
                
            
            finally:
                #muda o valor do campos de texto para "" (vazios)
                self.userLog.set_text("")
                self.senhaLog.set_text("")
                #fecha a tela de login e abre a do home principal
                self.login.hide()    
                self.homP = Homeprincipal ()
                self.login.destroy()
            
        #caso algum campo de entrada estiver vazio, abre-se a tela de alerta
        else:
            builder = Gtk.Builder()
            builder.add_from_file("visual/Alerta1.glade")
            self.alert = builder.get_object("tlAlerta1")  
            self.alert.show_all()
            builder.connect_signals(self) 
            self.alert.connect("destroy",Gtk.main_quit)
            Gtk.main()      
            #muda o valor do campos de texto para "" (vazios)
            self.userLog.set_text("")
            self.senhaLog.set_text("")
