# -*- coding: utf-8 -*-
'''
Feito em 2019
@description: Classe (visual) da Tela de cadastro do usuario, ao fazer o cadastro ela abre o home principal.
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["GetBook ", "Academicos: Waycon, Carliane e Kailane"]
__version__="1.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__="07/06/2019"

import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk,Gdk
from pacoteA.Cadastro import Cadastro
from controle.ControleCadastro import ControleCadastro
from visual.Homeprincipal import Homeprincipal


class CadastroTela:
    #Construtor
    #Instanciacao da tela de cadastro, da gtk builder...
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("visual/Cadastro.glade")
        #object's da tela de Cadastro
        self.cadastro = builder.get_object("tlCadastro")
        self.user = builder.get_object("tfUsr")
        self.email = builder.get_object("tfEmail")
        self.senha = builder.get_object("psSenha")
        self.confirmacao = builder.get_object("psConfirmacao")
        #execucao do metodo  css
        self.css()
        builder.connect_signals(self)
        self.cadastro.connect("destroy",Gtk.main_quit)
        self.cadastro.show_all()
        Gtk.main()
    #metodo da estilizacao da tela, css    
    def css(self):
        css = b"""
        .tlCadastro{
            background-color: #9ab6c8;
        }
        .gridCad{
            background-color: #fff;
            border-radius: 20px;
        
        }
        .botoes{
            border-radius: 50px;
        }
        .tlSucesso{
            background-color:#9ab6c8;
        }
        .tlAlerta{
            background-color: #fff;
            
        }
        
        """
        style_provider = Gtk.CssProvider() #instanciando css provider
        style_provider.load_from_data(css) 
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
    
    #sair da aplicacao
    def sair (self,evt):
        self.cadastro.destroy()

    #sair da tela de sucesso no cadastro      
    def sairSucesso (self, evt):
        self.sucesso.destroy()
        self.cadastro.destroy()
        
        
    #metodo do botao OK da tela de alerta, que fecha a tela de alerta
    def Ok(self, evt):
        self.alert.destroy()
   
    #esse metodo do botao home tem a funcao de feixar a janela de sucesso e de cadastro
    # e abir a tela de home principal
    def home(self, evt):
        self.cadastro.hide()
        self.sucesso.hide()
        self.cadastro.destroy()
        self.sucesso.destroy()
        self.homeP = Homeprincipal()
        
        
            
    def cadastrar (self, evt):
        #instancia da classe controle de Cadastro para a utilizacao dos metodos de verificacao de senha e campos
        con = ControleCadastro()
        
        #Condicao composta que primeiro verifica os campos de texto, se estiverem vazios nao possibilita a efetuacao do cadastro.
        #parametros: todos os campos da tela de cadastro
        if con.verificarCampos(self.user.get_text(), self.senha.get_text(), self.confirmacao.get_text(),self.email.get_text() ):
            #Caso os campos nao estejem vazios passa para a verificacao de senha
            #parametros: senha e confirmacao
            if con.verificarSenha(self.senha.get_text(), self.confirmacao.get_text()):
            #se as senhas estiverem iguais os objetos da classe cadastro (modelo) serao preenchidos
            
                try:
                    #instancia de cadastro(modelo)
                    dados = Cadastro()
                    dados.usr = (self.user.get_text())            
                    dados.email = (self.email.get_text())
                    dados.setSenha(self.senha.get_text())
                    dados.confirm = (self.confirmacao.get_text())
                    print("A senha do usuario Ã©: ",  dados.getSenha(), dados.usr)
            
                finally:
                    #os campos serao limpados e havera a aparicao de uma tela informando que o cadastro foi efetuado 
                    #dando possibilidade do usuario sair da aplicacao ou prosseguir indo para o home principal   
                    self.user.set_text("")
                    self.email.set_text("")
                    self.senha.set_text("")
                    self.confirmacao.set_text("")
                    #tela informando o sucesso do cadastro
                    builder = Gtk.Builder()a
                    builder.add_from_file("visual/Sucesso.glade")
                    self.sucesso = builder.get_object("tlSucesso")
                    builder.connect_signals(self)  
                    self.sucesso.show_all() 
                    self.sucesso.connect("destroy",Gtk.main_quit)
                    Gtk.main()
            #Caso as senhas nao forem iguais aparece uma tela de alerta
            else:
                #Tela de alerta de senhas diferentes
                builder = Gtk.Builder()
                builder.add_from_file("visual/Alerta2.glade")
                self.alert = builder.get_object("tlAlerta2")
                builder.connect_signals(self)  
                self.alert.show_all() 
                self.alert.connect("destroy",Gtk.main_quit)
                Gtk.main()
                #limpando campo de senha e confirmacao   
                self.senha.set_text("")
                self.confirmacao.set_text("")
            
                
        #Caso os campos estiverem vazios aparece uma tela de alerta        
        else:    
            builder = Gtk.Builder()
            builder.add_from_file("visual/Alerta1.glade")
            self.alert = builder.get_object("tlAlerta1")
            builder.connect_signals(self)  
            self.alert.show_all() 
            self.alert.connect("destroy",Gtk.main_quit)
            Gtk.main()
            #limpando campo de senha e confirmacao
            self.senha.set_text("")
            self.confirmacao.set_text("")
            
