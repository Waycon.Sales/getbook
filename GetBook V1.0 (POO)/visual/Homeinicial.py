# -*- coding: utf-8 -*-
'''
Feito em 2019
@description: Classe (visual) da tela de home INICIAL do app, ela conteem o botao de login, cadastro e um de sair. 
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["GetBook ", "Academicos: Waycon, Carliane e Kailane"]
__version__="1.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__="07/06/2019"

import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk,Gdk
from visual.CadastroTela import CadastroTela
from visual.LogarTela import LogarTela

class Homeinicial:
    #Construtor
    #Instancicao da tela do home inicial, da gtk builder...
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("visual/PgInicial.glade")
        self.homeI = builder.get_object("tlInicial")
        builder.connect_signals(self)
        self.css() #execucao do metodo css
        self.homeI.show_all()
        self.homeI.connect("destroy",Gtk.main_quit)
        Gtk.main()
    #metodo do botao sair, sai da aplicacao.
    def sair (self,evt):
        self.homeI.destroy()
    #metodo do botao cadastro tem a funcao de abrir a tela de cadastro e fechar a do home inicial
    def cadastro (self, evt):
        self.homeI.hide()
        self.homeI.destroy()
        self.cad = CadastroTela()
    #metodo do botao login tem a funcao de abrir a tela de login e fechar a do home inicial    
    def login (self,evt):
        self.homeI.hide()
        self.homeI.destroy()
        self.log = LogarTela() 
    #metodo da estilizacao da tela, css    
    def css(self):
        css = b"""
        .tlInicial{
            background-color: #9ab6c8;
            
        }
        .btCadastro{
            border-radius: 50px;
        }
        .btLogin{
            border-radius: 50px;
        }
        .btSair{
            border-radius: 50px;
            
        }
        .lbLog{
            background-color: #aab6c9;
            border: solid 2px #92b4c9;
            border-radius:7px;
            
        
        }
        .lbCad{
            background-color: #aab6c9;
            border: solid 2px #92b4c9;
            border-radius:7px;
            
        
        }
        """
        style_provider = Gtk.CssProvider() #instanciando css provider
        style_provider.load_from_data(css) 
        Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(),
        style_provider,Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        
        
