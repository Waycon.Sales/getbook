# -*- coding: utf-8 -*-
'''
Feito em 2019
@description: Classe (visual) da tela de Aluguel De Livros nessa tela encontra-se o formulario onde serao colocadas as informacoes do Aluno e a tabela onde serao mostradas. 
@author: Waycon Sales

'''
__author__="Waycon Sales"
__credits__=["GetBook ", "Academicos: Waycon, Carliane e Kailane"]
__version__="1.0"
__email__="antoniowaycon@gmail.com"
__status__="terminado (producao)"
__date__="07/06/2019"

import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk,Gdk
from pacoteA.Aluguel import Aluguel
from controle.AluguelControle import AluguelControle

class AluguelLivro():
    #construtor
    def __init__(self):
        #instanciacao da Gtk
        builder = Gtk.Builder()
        #adiciona o arquivo do glade
        builder.add_from_file("visual/AluguelTabela.glade")
        #objetos do formulario
        self.aluguel = builder.get_object("tlAluguelTabela")
        self.nome = builder.get_object("tfNome")
        self.titulo = builder.get_object("tfTitulo")
        self.editora = builder.get_object("tfEditora")
        self.email = builder.get_object("tfEmail")
        self.tellAluno = builder.get_object("tfTelAluno")
        self.tellPais= builder.get_object("tfTelPais")
        self.autor = builder.get_object("tfAutor")
        self.dataEntrega = builder.get_object("tfEntrega")
        self.dataEmissao = builder.get_object("tfEmissao")
        self.curso = builder.get_object("tfCurso")
        self.serie = builder.get_object("tfSerie")
        #object's da Tabela
        self.nomeTabela = builder.get_object("lbAluno1")
        self.tituloTabela = builder.get_object("lbTitulo1")
        self.editoraTabela = builder.get_object("lbEditora1")
        self.emailTabela = builder.get_object("lbEmail1")
        self.tellAlunoTabela = builder.get_object("lbTelAluno1")
        self.tellPaisTabela= builder.get_object("lbTelPais1")
        self.autorTabela = builder.get_object("lbAutor1")
        self.dataEntregaTabela = builder.get_object("lbEntrega1")
        self.dataEmissaoTabela = builder.get_object("lbEmissao1")
        self.cursoTabela = builder.get_object("lbCurso1")
        self.serieTabela = builder.get_object("lbSerie1")
        #pega os sinais dos botoes
        builder.connect_signals(self)
        #execucao do metodo  css
        self.css()
        self.aluguel.show_all()
        self.aluguel.connect("destroy",Gtk.main_quit)
        Gtk.main()
    #metodo da estilizacao da tela, css  
    def css(self):
        css = b"""
        .tlAluguelTabela{
            background-color: #9ab6c8;
            
        }
        .botoes{
            border-radius: 50px;
        }
        .gridLivro{
            background-color: #fff;
            border-radius: 20px;
            
        }
        .gridAluno{
            background-color: #fff;
            border-radius: 20px;
            
        }
        .gridDatas{
            background-color: #fff;
            border-radius: 20px;
            
        }
        .tlAlerta{
            background-color: #fff;
            
        }
        .tlRenovacao{
            background-color: #9ab6c8;
        
        }
        .gridDataNova{
            background-color: #fff;
            border-radius: 50px;
            
        }
        
        """
        style_provider = Gtk.CssProvider() #instanciando css provider
        style_provider.load_from_data(css) 
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            )
        
    #metodo do botao devolvido.
    def devolvido(self,evt):
    
    #Ao clicar em devolvido obotao ira realizar a acao de mudar o conteudo da tabela para ""(vazio)
    
        self.serieTabela.set_text("")
        self.cursoTabela.set_text("")
        self.nomeTabela.set_text("")
        self.emailTabela.set_text("")
        self.tellAlunoTabela.set_text("")
        self.tellPaisTabela.set_text("")
        self.tituloTabela.set_text("")
        self.autorTabela.set_text("")
        self.editoraTabela.set_text("")
        self.dataEmissaoTabela.set_text("")
        self.dataEntregaTabela.set_text("")
    
    #metodo do botao sair.
    def sair (self,evt):
        self.aluguel.destroy()
        #sair da aplicacao
        
    #metodo do botao renovacao, abre a tela de mudanca da data de entrega
    def renovacao(self,evt):
        builder = Gtk.Builder()
        builder.add_from_file("visual/Renovar.glade")
        self.renovar = builder.get_object("tlRenovacao")
        self.dataNova = builder.get_object("tfDataNova")
        builder.connect_signals(self)
        self.renovar.show_all()
        self.renovar.connect("destroy",Gtk.main_quit)
        Gtk.main()
    
    #metodo do botao voltar da tela de renovar a data de Entrega
    def voltar (self, evt):
        self.renovar.destroy()
        
    #metodo do botao enviar que envia o novo valor da data de Entrega
    def enviarRenovacao (self, evt):
        con = AluguelControle()
        
        if con.verificarCampoRenovacao(self.dataNova.get_text()):
            #label de entrega data serao modificado com a nova data
            self.dataEntregaTabela.set_text(self.dataNova.get_text())
            self.renovar.destroy()
        #caso os campos de entrada estejam vazios
        else:
            #abre-se a tela de alerta
            builder = Gtk.Builder()
            builder.add_from_file("visual/Alerta1.glade")
            self.alert = builder.get_object("tlAlerta1")
            builder.connect_signals(self)  
            self.alert.show_all() 
            self.alert.connect("destroy",Gtk.main_quit)
            Gtk.main()
            
    #metodo do botao OK da tela de alerta, que fecha a tela de alerta    
    def Ok (self, evt):
        self.alert.destroy()
    #metodo do botao efetuar que envia todo o conteudo digitado pelo usuario para a tabela.
    def efetuar (self, evt):
        #instanciacao da classe de AluguelControle, para a utilizacao do metodo de verificar campo de senha.
        con = AluguelControle()
        #condicao que contem a execucao do metodo da controle recebe os valores que estao nos inputs do formulario como argumentos
        #parametros: todos os campos de entrada da tela de aluguel
        if con.verificarCampos(self.nome.get_text(), self.serie.get_text(),
         self.curso.get_text(), self.email.get_text(), self.tellAluno.get_text(),
         self.tellPais.get_text(), self.titulo.get_text(), self.autor.get_text(), 
         self.editora.get_text(),self.dataEmissao.get_text(),
         self.dataEntrega.get_text()):
            #estrutura que corrige os erros
            try:
                #instancia da classe modelo Aluguel, onde as variaveis recebem os valores que estao nos seus respectivos campos de texto
                #Assim e possivel limpar tais campos para a efetuacao de dados  
                
                alu = Aluguel((self.nome.get_text()),(self.curso.get_text()))#Construtor da superclase de aluguel(ele recebe o nome e o curso)
                alu.serie = (self.serie.get_text())
                alu.email = (self.email.get_text())
                alu.dataInicial = (self.dataEmissao.get_text())
                alu.setDataFinal(self.dataEntrega.get_text())
                alu.livro.nomeLivro = (self.titulo.get_text())
                alu.livro.autor = (self.autor.get_text())
                alu.livro.editora=(self.editora.get_text())
                alu.telefone.numeroAluno=(self.tellAluno.get_text())
                alu.telefone.numeroResponsavel=(self.tellPais.get_text())
            
                #Os labels que compoem a tabela sao preenchidos com os valores das variaveis da classe modelo de aluguel. 
                
                self.nomeTabela.set_text(str(alu.nome))
                self.serieTabela.set_text(str(alu.serie))
                self.cursoTabela.set_text(str(alu.curso))
                self.emailTabela.set_text(str(alu.email))
                self.tellAlunoTabela.set_text(str(alu.telefone.numeroAluno))
                self.tellPaisTabela.set_text(str(alu.telefone.numeroResponsavel))
                self.tituloTabela.set_text(str(alu.livro.nomeLivro))
                self.autorTabela.set_text(str(alu.livro.autor))
                self.editoraTabela.set_text(str(alu.livro.editora))
                self.dataEmissaoTabela.set_text(str(alu.dataInicial))
                self.dataEntregaTabela.set_text(str(alu.getDataFinal()))
             
            finally:    
                #apaga as informacoes dos campos de texto do formulario
                self.nome.set_text("")
                self.serie.set_text("")
                self.curso.set_text("")
                self.email.set_text("")
                self.tellAluno.set_text("")
                self.tellPais.set_text("")
                self.titulo.set_text("")
                self.autor.set_text("")
                self.editora.set_text("")
                self.dataEmissao.set_text("")
                self.dataEntrega.set_text("")
        #caso os campos de entrada estejam vazios
        else:
            #abre-se a tela de alerta
            builder = Gtk.Builder()
            builder.add_from_file("visual/Alerta1.glade")
            self.alert = builder.get_object("tlAlerta1")
            builder.connect_signals(self)  
            self.alert.show_all() 
            self.alert.connect("destroy",Gtk.main_quit)
            Gtk.main()
            
