# Aluguel de livros na  Biblioteca
A implementação de avaliações baseadas em livros paradidáticos, tais escolhidos pelo docente, acarreta em um crescimento no aluguel das obras. O software possibilita uma facilitação na hora de registrar as informações necessárias para o aluguel de livros na biblioteca. Conta com o recebimento de dados onde podem ser preenchidos pelos alunos enquanto os bibliotecários estiverem desempenhando outras funções. 

## Versão em PYTHON
## Funcionalidades 

#### Versão 1.0 (POO - MVC) Aluguel de livros:

    * Registro de alunos
    * Devolução
    * Renovação

#### Versão 2.0 (implementação de banco de dados)
    
    * Sistema de Login e Cadastro
    * Insere os registro dos emprestimos de livros no banco de dados. 
    * Seleciona os dados do banco de dados e mostra em tabela.
    * Atualiza a data de entrega.
    * Apaga dados do emprestimo de livros no banco de dados.

A ideia do software foi elaborada por uma equipe (Antonio Waycon, Carliane Cavalcante e Kailane Fonteles). Porém a versão em python foi executada por Antonio Waycon e a versão em  Java por Carliane e Kailane.
